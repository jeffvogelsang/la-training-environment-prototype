#
# Cookbook Name:: jenkins
# Recipe:: default
#
# Copyright 2015, LeadingAgile, Jeff Vogelsang
#
# All rights reserved - Do Not Redistribute
#

user 'jenkins' do
  comment 'Jenkins'
  home '/opt/jenkins'
  system true
  shell '/bin/bash'
end

directory "/opt/jenkins" do
  owner "jenkins"
  group "jenkins"
  mode 00755
  action :create
end

# This syntax copies the file from the guest to another location on the guest.
remote_file "Copy Jenkins WAR" do
  path "/opt/jenkins/jenkins.war"
  source "file:///vagrant/tools/jenkins.war"
  owner 'jenkins'
  group 'jenkins'
  mode 0644
end

# Upstart

cookbook_file "/etc/init/jenkins.conf" do
  source "jenkins.conf"
  owner "root"
  group "root"
  mode 00644
end

bash 'register-jenkins-upstart-service' do
  user "root"
  code "initctl reload-configuration"
end

# SystemD

# cookbook_file "/lib/systemd/system/jenkins.service" do
#   source "jenkins.service"
#   owner "root"
#   group "root"
#   mode 00644
# end
#
# bash 'register-jenkins-systemd-service' do
#   user "root"
#   code "systemctl daemon-reload"
# end
#
# bash 'enable-jenkins-systemd-service' do
#   user "root"
#   code "systemctl enable jenkins"
# end

service 'jenkins' do
  action :restart
end
