#
# Cookbook Name:: vmsetup
# Recipe:: default
#
# Copyright 2015, LeadingAgile, Jeff Vogelsang
#
# All rights reserved - Do Not Redistribute
#

# We don't need to be perfectly up to date over a recent image baseline... -jjv

# bash "update-apt-package-list" do
#   user "root"
#   cwd "/tmp"
#   code "apt-get update"
# end

# bash "upgrade-apt-packages" do
#   user "root"
#   cwd "/tmp"
#   code "apt-get -y upgrade"
# end

# apt_package "build-essential" do
#   action :install
# end

# apt_package "git" do
#   action :install
# end

bash 'install-git' do
  user "root"
  cwd "/tmp"
  code <<-EOH
    dpkg -i /vagrant/tools/liberror-perl_0.17-1.1_all.deb || true
    dpkg -i /vagrant/tools/git-man_1.9.1-1ubuntu0.1_all.deb || true
    dpkg -i /vagrant/tools/git_1.9.1-1ubuntu0.1_amd64.deb || true
    EOH
end

bash 'install-atom' do
  user "root"
  cwd "/tmp"
  code <<-EOH
    dpkg -i /vagrant/tools/atom-amd64.deb || true
    EOH
end

cookbook_file "/home/vagrant/launcher-backup.txt" do
  source "launcher-backup.txt"
  owner "vagrant"
  group "vagrant"
  mode 00644
end

cookbook_file "/home/vagrant/.config/upstart/fix-launcher.conf" do
  source "fix-launcher.conf"
  owner "vagrant"
  group "vagrant"
  mode 00644
end
