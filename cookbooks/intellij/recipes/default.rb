#
# Cookbook Name:: intellij
# Recipe:: default
#
# Copyright 2015, LeadingAgile, Jeff Vogelsang
#
# All rights reserved - Do Not Redistribute
#

directory "/opt/intellij" do
  owner "root"
  group "root"
  mode 00755
  action :create
end

bash "extract-intellij" do
  user "root"
  cwd "/tmp"
  code "tar -xvvzf /vagrant/tools/ideaIU-14.1.5.tar.gz -C /tmp"
end

bash "move-intellij-files" do
  user "root"
  cwd "/tmp"
  code "sudo rsync -rK --ignore-existing /tmp/idea-IU-141.2735.5/ /opt/intellij/"
end

bash "fix-intellij-files-permissions" do
  user "root"
  cwd "/tmp"
  code "sudo chown -R root.root /opt/intellij"
end

cookbook_file "/usr/share/applications/jetbrains-idea.desktop" do
  source "jetbrains-idea.desktop"
  owner "root"
  group "root"
  mode 00644
end
