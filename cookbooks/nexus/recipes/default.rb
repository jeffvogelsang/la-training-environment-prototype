#
# Cookbook Name:: jenkins
# Recipe:: default
#
# Copyright 2015, LeadingAgile, Jeff Vogelsang
#
# All rights reserved - Do Not Redistribute
#

# For reference: https://books.sonatype.com/nexus-book/reference/install-sect-service.html

user 'nexus' do
  comment 'Nexus'
  home '/opt/nexus'
  system true
  shell '/bin/bash'
end

directory "/opt/nexus" do
  owner "nexus"
  group "nexus"
  mode 00755
  action :create
end

directory "/opt/sonatype-work" do
  owner "nexus"
  group "nexus"
  mode 00755
  action :create
end

directory "/opt/sonatype-work/nexus" do
  owner "nexus"
  group "nexus"
  mode 00755
  action :create
end

directory "/opt/sonatype-work/run" do
  owner "nexus"
  group "nexus"
  mode 00755
  action :create
end

bash "unzip-nexus" do
  user "root"
  cwd "/tmp"
  code "sudo unzip -o /vagrant/tools/nexus-2.11.4-01-bundle.zip -d /tmp"
end

bash "move-nexus-files" do
  user "root"
  cwd "/tmp"
  # code "sudo cp -rn /tmp/nexus-2.11.4-01/* /opt/nexus"
  code "sudo rsync -rK --ignore-existing /tmp/nexus-2.11.4-01/ /opt/nexus/"
end

bash "fix-nexus-files-permissions" do
  user "root"
  cwd "/tmp"
  code "sudo chown -R nexus.nexus /opt/nexus"
end

cookbook_file "/opt/nexus/conf/nexus.properties" do
  source "nexus.properties"
  owner "root"
  group "root"
  mode 00755
end

cookbook_file "/etc/init.d/nexus" do
  source "nexus"
  owner "root"
  group "root"
  mode 00755
end

service "nexus" do
  supports :restart => true, :start => true, :stop => true, :reload => true
  action :restart
end

# bash "install-nexus-service" do
#   user "root"
#   cwd "/etc/init.d"
#   code "update-rc.d nexus defaults"
# end

# service 'nexus' do
#   action :restart
# end
