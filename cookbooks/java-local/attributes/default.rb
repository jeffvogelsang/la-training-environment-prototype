override['java']['install_flavor'] = "oracle"
override['java']['jdk_version'] = '8'
override['java']['jdk']['8']['x86_64']['url'] = 'http://localhost:5000/tools/jdk-8u60-linux-x64.tar.gz'
override['java']['oracle']['accept_oracle_download_terms'] = true
# SHA256 checksum
override['java']['jdk']['8']['x86_64']['checksum'] = 'ebe51554d2f6c617a4ae8fc9a8742276e65af01bd273e96848b262b3c05424e5'
# MD5 checksum
# override['java']['jdk']['8']['x86_64']['checksum'] = 'b8ca513d4f439782c019cb78cd7fd101'
