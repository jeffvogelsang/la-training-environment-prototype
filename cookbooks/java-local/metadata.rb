name             'java-local'
maintainer       'LeadingAgile'
maintainer_email 'jeff.vogelsang@leadingagile.com'
license          'All rights reserved'
description      'Wraps the java cookbook; installs from a local tarball.'
# long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'java'
