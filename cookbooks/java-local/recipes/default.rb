#
# Cookbook Name:: java-local
# Recipe:: default
#
# Copyright 2015, LeadingAgile, Jeff Vogelsang
#
# All rights reserved - Do Not Redistribute
#

# NOTES:
# 1.) attributes for jave recipe overriden in this recipe's
#       default attributes ../attributes/default.rb
# 2.) this recipe relies on the java jdk file is available
#       on an http server running locally on port 5000. This
#       server is provided by the http-repo-start/stop recipes.

include_recipe 'java'
