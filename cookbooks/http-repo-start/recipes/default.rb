#
# Cookbook Name:: http-repo-start
# Recipe:: default
#
# Copyright 2015, LeadingAgile, Jeff Vogelsang
#
# All rights reserved - Do Not Redistribute
#

bash "start-fake-repository" do
  user "root"
  cwd "/vagrant"
  code "python -m SimpleHTTPServer 5000 > /dev/null 2>&1 &"
end
