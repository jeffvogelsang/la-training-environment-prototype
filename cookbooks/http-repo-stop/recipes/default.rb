#
# Cookbook Name:: http-repo-stop
# Recipe:: default
#
# Copyright 2015, LeadingAgile, Jeff Vogelsang
#
# All rights reserved - Do Not Redistribute
#

bash "stop-fake-repository" do
  user "root"
  cwd "/vagrant"
  code "kill `ps -ef | awk '/[S]impleHTTPServer/{print $2}'`"
end
