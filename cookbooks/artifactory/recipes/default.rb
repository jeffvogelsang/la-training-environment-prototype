#
# Cookbook Name:: artifactory
# Recipe:: default
#
# Copyright 2015, LeadingAgile, Jeff Vogelsang
#
# All rights reserved - Do Not Redistribute
#

user 'artifactory' do
  comment 'Artifactory'
  home '/opt/artifactory'
  system true
  shell '/bin/bash'
end

directory "/opt/artifactory" do
  owner "artifactory"
  group "artifactory"
  mode 00755
  action :create
end

bash "unzip-artifactory" do
  user "root"
  cwd "/tmp"
  code "sudo unzip -o /vagrant/tools/jfrog-artifactory-oss-4.1.3.zip -d /tmp"
end

bash "copy-artifactory-files" do
  user "root"
  cwd "/tmp"
  # code "sudo cp -rnL /tmp/artifactory-oss-4.1.3/* /opt/artifactory"
  code "sudo rsync -rK --ignore-existing /tmp/artifactory-oss-4.1.3/ /opt/artifactory/"
end

bash "fix-artifactory-files-permissions" do
  user "root"
  cwd "/tmp"
  code "sudo chown -R artifactory.artifactory /opt/artifactory"
end

cookbook_file "/opt/artifactory/tomcat/conf/server.xml" do
  source "server.xml"
  owner "artifactory"
  group "artifactory"
  mode 00644
end

bash "install-artifactory-service" do
  user "root"
  cwd "/tmp"
  code "/opt/artifactory/bin/installService.sh"
end

cookbook_file "/etc/opt/jfrog/artifactory/default" do
  source "default"
  owner "artifactory"
  group "artifactory"
  mode 00644
end

service "artifactory" do
  action :restart
end
