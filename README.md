# LeadingAgile Training Environment Prototype

LATE provides a mostly self-contained development environment that can either be cloned to set up a lab, or examined to understand how to utilize configuration management tools. It tries to avoid any requirements for downloading dependencies from the Internet during build time. This repo represents a prototype. Fork this and customize to meet your specific needs.

## Provides
- Java
- IntelliJ
- Artifactory
- Jenkins
- (Optionally) Nexus
- Atom

## Setup Requirements

Vagrant & VirtualBox must be installed on the host.

Vagrant needs a basebox to configure. It will download an Ubuntu box from Atlas/Boxcutter on first run. Alternatively, the box can be provided by extracting a copy to Vagrant's configuration root in the boxes directory. The current base box is: *box-cutter-VAGRANTSLASH-ubuntu1404-desktop*

## Build-time Requirements

The tools directory must be populated with a set of binaries that would normally get pulled from internet sources at build time. These are hard-coded in the Chef cookbooks to ensure reproducibility without internet connectivity.

- atom-amd64.deb
- chef_12.5.1-1_amd64.deb
- git-man_1.9.1-1ubuntu0.1_all.deb
- git_1.9.1-1ubuntu0.1_amd64.deb
- ideaIU-14.1.5.tar.gz
- jdk-8u60-linux-x64.tar.gz
- jenkins.war
- jfrog-artifactory-oss-4.1.3.zip
- liberror-perl_0.17-1.1_all.deb
- nexus-2.11.4-01-bundle.zip

## Downloads

The above requirements are staged on Dropbox here:

https://www.dropbox.com/sh/ha0rmh4vk8eu9zf/AACxXSh2Y5JsCs_MFlQwbv9Fa?dl=0
