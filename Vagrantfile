# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

# Use Ubuntu 14.04.x LTS from canonical; by URL download.
# VM_BOX = "trusty-server-cloudimg-amd64-vagrant"
# VM_BOX_URL = "http://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"

# Use a Box Cutter version of Ubuntu that provides a desktop.
# Note: Different distros use different init systems; e.g. upstart, systemd
# Ubuntu 14.04 LTS /Trusty Tahr
VM_BOX = "box-cutter/ubuntu1404-desktop"
# Ubuntu 15.04 / Vivid Vervet
# VM_BOX = "box-cutter/ubuntu1504-desktop"
# CentoOS 7.1
# VM_BOX = "box-cutter/centos71-desktop"

# Script to bootstrap Chef
$chef_install = <<SCRIPT

    # Ubuntu
    sudo dpkg -i /vagrant/tools/chef_12.5.1-1_amd64.deb

    # CentOS
    # sudo rpm -Uvh /vagrant/tools/chef-12.5.1-1.el7.x86_64.rpm

    # Universal/Remote
    # curl -L https://www.opscode.com/chef/install.sh | sudo bash

    exit 0
SCRIPT

# NOTE: All configurations here assume a 64bit host and guest platform!
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = VM_BOX

  # If we are downloading from a URL, we need to specify here.
  # config.vm.box = VM_BOX
  # config.vm.box_url = VM_BOX_URL

  # Make guest ports available to the host
  config.vm.network "forwarded_port", guest: 8081, host: 8081
  config.vm.network "forwarded_port", guest: 8082, host: 8082
  config.vm.network "forwarded_port", guest: 8083, host: 8083

  # Update VirtualBox guest extensions if we have the guest updater plugin
  # See: https://github.com/dotless-de/vagrant-vbguest
  # config.vbguest.auto_update = false

  # Specify VirtualBox parameters: Name, Memory, CPU, GUI
  config.vm.provider :virtualbox do |vb|
	  vb.name = 'vagrant-bc3-training'
    vb.gui = false
    vb.customize ["modifyvm", :id, "--memory", 2048]
    vb.customize ["modifyvm", :id, "--cpus", 2]
  end

  # Bootstrap Chef on the Vagrant machine.
  config.vm.provision "shell",
      inline: $chef_install

  # Run our Chef cookbooks!
  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = "./cookbooks"
    chef.add_recipe "http-repo-start"
    chef.add_recipe "java-local"
    chef.add_recipe "jenkins"
    chef.add_recipe "artifactory"
    # chef.add_recipe "nexus"
    chef.add_recipe "intellij"
    chef.add_recipe "vmsetup"
    chef.add_recipe "http-repo-stop"
  end

end
